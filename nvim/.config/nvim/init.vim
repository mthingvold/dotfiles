filetype plugin on
syntax on
set mouse=a

set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set colorcolumn=120

call plug#begin()
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fireplace'
Plug 'scrooloose/syntastic'
Plug 'tpope/vim-surround'
Plug 'bling/vim-airline'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-airline/vim-airline-themes'
Plug 'dylanaraps/wal.vim'
Plug 'dim13/smyck.vim'
Plug 'vimwiki/vimwiki'
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'
Plug 'Shougo/deoplete.nvim'
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'junegunn/seoul256.vim'
call plug#end()

colorscheme wal
"colo seoul256
set number

let g:deoplete#enable_at_startup = 1

" rust stuff
au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap <leader>gd <Plug>(rust-doc)
let g:syntastic_rust_checkers = ['cargo']
let g:syntastic_c_checkers = ['make']


" set makeprg=make\ -C\ ../build\ -j9
nnoremap <F4> :w<cr> :!gcc % <cr>
nnoremap <F5> :!./a.out <cr>
" set cut+paste measures
map <F7> :w !xclip<CR><CR>
vmap <F7> "*y
map <S-F7> :r!xclip -o<CR>
"set a way to make
nnoremap <F9> :!make % <cr>
vnoremap <C-c> "+y

" vimwiki stuff
let vim_markdown_preview_browser = 'firefox'
